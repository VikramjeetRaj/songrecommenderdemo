package com.songrecommender.recommender.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Trie implements Serializable {
    private static final long serialVersionUID = 1L;
    private TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    public void insert(String key) {
        TrieNode t = root;
        for(int i = 0; i < key.length(); i++) {
            char index = key.charAt(i);
            HashMap<Character, TrieNode> children = t.getChildren();
            if(children !=null && !children.containsKey(index)) {
                children.put(index, new TrieNode(index));
                t.setChildren(children);
            }
            t = t.getChildren().get(index);
        }
        t.setEnd(true);
        int matchVal = key.split(",").length * 10;
        if(matchVal == 10) {
            if(key.split(":")[0].equalsIgnoreCase("genre")) {
                matchVal+=3;
            }
            else if(key.split(":")[0].equalsIgnoreCase("singer")) {
                matchVal+=2;
            }
            else {
                matchVal+=1;
            }
        }
        t.setMatchVal(matchVal);
    }

    public int search(String key) {
        TrieNode t = root;
        for(int i = 0; i < key.length(); i++) {
            char index = key.charAt(i);
            HashMap<Character, TrieNode> children = t.getChildren();
            if(!children.containsKey(index)) {
                return t.getMatchVal();
            } else {
                t = children.get(index);
            }
        }

        if(t == null) {
            return -1;
        }

        if(t.isEnd()) {
            return t.getMatchVal();
        }

        return -1;
    }

    private void display(TrieNode t, String str, List<String> s) {
        if (t.isEnd()) {
            s.add("["+str+"]");
        } 
            for(char ch:t.getChildren().keySet()) {
                display(t.getChildren().get(ch), str.concat(Character.toString(ch)), s);
            
        }
    }

    @Override
    public String toString() {
        List<String> s = new LinkedList<String>();
        String str = "";
        display(root, str, s);
        return s.toString();
    }

    
}