package com.songrecommender.recommender.util;

import java.io.Serializable;
import java.util.HashMap;

public class TrieNode implements Serializable {
    private static final long serialVersionUID = 2L;
    private HashMap<Character, TrieNode> children = new HashMap<Character, TrieNode>();
    private boolean isEnd;
    private int matchVal;
    char c;

    public TrieNode() {
        isEnd = false;
        matchVal = -1;
    }

    public TrieNode(char c) {
        isEnd = false;
        matchVal = -1;
        this.c = c;
    }

    public HashMap<Character, TrieNode> getChildren() {
        return children;
    }

    public void setChildren(HashMap<Character, TrieNode> children) {
        this.children = children;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean isEnd) {
        this.isEnd = isEnd;
    }

    public int getMatchVal() {
        return matchVal;
    }

    public void setMatchVal(int matchVal) {
        this.matchVal = matchVal;
    }
}