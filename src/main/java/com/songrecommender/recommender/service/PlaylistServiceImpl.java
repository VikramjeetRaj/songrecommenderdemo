package com.songrecommender.recommender.service;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.songrecommender.recommender.model.Playlist;
import com.songrecommender.recommender.model.Song;
import com.songrecommender.recommender.repository.PlaylistRepository;
import com.songrecommender.recommender.repository.SongRepository;
import com.songrecommender.recommender.util.Trie;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    private static final Logger LOGGER=LoggerFactory.getLogger(PlaylistServiceImpl.class);


    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    private SongRepository songRepository;

    private List<String> getAttributeCombo(final Song song) {
        final List<String> attributes = new LinkedList<String>();
        attributes.add(
                "genre:" + song.getGenre() + "," + "singer:" + song.getSinger() + "," + "tempo:" + song.getTempo());
        attributes.add("genre:" + song.getGenre() + "," + "singer:" + song.getSinger());
        attributes.add("singer:" + song.getSinger() + "," + "tempo:" + song.getTempo().toString());
        attributes.add("genre:" + song.getGenre() + "," + "tempo:" + song.getTempo().toString());
        attributes.add("genre:" + song.getGenre());
        attributes.add("tempo:" + song.getTempo().toString());
        attributes.add("singer:" + song.getSinger());
        return attributes;
    }

    private Trie getUpdatedMatchTrie(final Song song, final Playlist playlist) {
        Trie matchTrie;
        if (playlist.getTrie() == null) {
            matchTrie = new Trie();
        } else {
            matchTrie = playlist.getTrie();
        }
        for (final String attr : getAttributeCombo(song)) {
            matchTrie.insert(attr);
        }
        playlist.setTrie(matchTrie);
        return matchTrie;
    }

    private int getMatchScore(final Song song, final Playlist playlist) {
        Trie matchTrie;
        matchTrie = playlist.getTrie();
        int max = Integer.MIN_VALUE;
        for (final String key : getAttributeCombo(song)) {
            max = Math.max(matchTrie.search(key), max);
        }
        return max;
    }

    private double calculateOnBasisOfCount(final Song song, final Playlist p) {
        final int genreCount = p.getCount().containsKey(song.getGenre()) ? p.getCount().get(song.getGenre()) : 0;
        final int singerCount = p.getCount().containsKey(song.getSinger()) ? p.getCount().get(song.getSinger()) : 0;
        final int tempoCount = p.getCount().containsKey(song.getTempo().toString())
                ? p.getCount().get(song.getTempo().toString())
                : 0;
        int max = Math.max(Math.max(genreCount, singerCount), tempoCount);
        if (max == genreCount)
            max += 3;
        if (max == singerCount)
            max += 2;
        else
            max += 1;
        return 20 + ((double) max / 1000000);
    }

    @Override
    public Playlist createPlaylist(final Playlist playlist) {
        Playlist p = null;
        Trie matchTrie = null;
        final Map<String, Integer> count = playlist.getCount();
        for (final Song song : playlist.getSongs()) {
            matchTrie = getUpdatedMatchTrie(song, playlist);
            if (count.containsKey(song.getGenre())) {
                count.put(song.getGenre(), count.get(song.getGenre()) + 1);
            } else {
                count.put(song.getGenre(), 1);
            }

            if (count.containsKey(song.getSinger())) {
                count.put(song.getSinger(), count.get(song.getSinger()) + 1);
            } else {
                count.put(song.getSinger(), 1);
            }

            if (count.containsKey(song.getTempo().toString())) {
                count.put(song.getTempo().toString(), count.get(song.getTempo().toString()) + 1);
            } else {
                count.put(song.getTempo().toString(), 1);
            }
        }
        playlist.setTrie(matchTrie);
        playlist.setCount(count);
        p = playlistRepository.save(playlist);
        return p;
    }

    @Override
    public Playlist addSongToPlaylist(final Long id, final Song song) {
        final Optional<Playlist> optionalP = playlistRepository.findById(id);
        Playlist p = optionalP.isPresent() ? optionalP.get() : null;
        final Set<Song> songs = p.getSongs();
        songs.add(song);
        p.setSongs(songs);
        p.setTrie(getUpdatedMatchTrie(song, p));
        final Map<String, Integer> count = p.getCount();
        if (count.containsKey(song.getGenre())) {
            count.put(song.getGenre(), count.get(song.getGenre()) + 1);
        } else {
            count.put(song.getGenre(), 1);
        }

        if (count.containsKey(song.getSinger())) {
            count.put(song.getSinger(), count.get(song.getSinger()) + 1);
        } else {
            count.put(song.getSinger(), 1);
        }

        if (count.containsKey(song.getTempo().toString())) {
            count.put(song.getTempo().toString(), count.get(song.getTempo().toString()) + 1);
        } else {
            count.put(song.getTempo().toString(), 1);
        }
        p.setCount(count);
        p = playlistRepository.save(p);
        return p;
    }

    @Override
    public Playlist showPlaylist(final Long id) {
        final Optional<Playlist> optionalP = playlistRepository.findById(id);
        final Playlist p = optionalP.isPresent() ? optionalP.get() : null;
        return p;
    }

    @Override
    public List<Song> getRecommendation(final Long pId) {
        final Optional<Playlist> optionalP = playlistRepository.findById(pId);
        final Playlist p = optionalP.isPresent() ? optionalP.get() : null;
        final Map<Double, List<Song>> orderMap = new TreeMap<Double, List<Song>>(Comparator.reverseOrder());
        for (final Song song : songRepository.findAll()) {
            if (!p.getSongs().contains(song)) {
                double matchScore = getMatchScore(song, p);
                if (matchScore == 20D) {
                    matchScore = calculateOnBasisOfCount(song, p);
                }
                LOGGER.info(matchScore + " :------------- ");
                LOGGER.info(song.toString());
                final List<Song> value = orderMap.containsKey(matchScore) ? orderMap.get(matchScore)
                        : new LinkedList<Song>();
                value.add(song);
                orderMap.put(matchScore, value);
            }
        }
        LOGGER.info(orderMap.toString());
        final List<Song> ans = orderMap.values().stream().flatMap(x -> x.stream()).collect(Collectors.toList());
        return ans;
    }

}