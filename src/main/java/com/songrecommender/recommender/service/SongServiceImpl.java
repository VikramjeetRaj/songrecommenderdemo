package com.songrecommender.recommender.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.songrecommender.recommender.model.Song;
import com.songrecommender.recommender.repository.SongRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SongServiceImpl implements SongService {

    @Autowired 
    private SongRepository songRepository;

    @Override
    public Song save(Song song) {
        return songRepository.save(song);
    }

    @Override
    public List<Song> saveAll(List<Song> songs) {
        return StreamSupport.stream(songRepository.saveAll(songs).spliterator(), false).collect(Collectors.toList());
    }

}