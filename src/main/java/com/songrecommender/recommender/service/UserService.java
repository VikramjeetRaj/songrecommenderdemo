package com.songrecommender.recommender.service;

import com.songrecommender.recommender.model.User;

public interface UserService {
    User save(User user);
}