package com.songrecommender.recommender.service;

import com.songrecommender.recommender.model.User;
import com.songrecommender.recommender.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired 
    private UserRepository userRepository;

    @Override
    public User save(final User user) {
        return userRepository.save(user);
    }
    
}