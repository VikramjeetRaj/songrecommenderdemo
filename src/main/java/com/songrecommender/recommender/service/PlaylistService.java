package com.songrecommender.recommender.service;

import java.util.List;

import com.songrecommender.recommender.model.Playlist;
import com.songrecommender.recommender.model.Song;

public interface PlaylistService {
    Playlist createPlaylist(Playlist playlist);
    Playlist addSongToPlaylist(Long id, Song song);
    Playlist showPlaylist(Long playlistId);
    List<Song> getRecommendation(Long id);
}