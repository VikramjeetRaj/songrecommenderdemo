package com.songrecommender.recommender.service;

import java.util.List;

import com.songrecommender.recommender.model.Song;

public interface SongService {
    Song save(Song song);
    List<Song> saveAll(List<Song> songs);
}