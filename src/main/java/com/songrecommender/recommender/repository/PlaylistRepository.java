package com.songrecommender.recommender.repository;

import java.util.Optional;

import com.songrecommender.recommender.model.Playlist;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaylistRepository extends CrudRepository<Playlist, Long> {
    
    <S extends Playlist> S save(S entity);
    Optional<Playlist> findById(Long id);
    Iterable<Playlist> findAll();
}