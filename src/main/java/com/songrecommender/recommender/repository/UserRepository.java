package com.songrecommender.recommender.repository;

import java.util.List;

import com.songrecommender.recommender.model.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    <S extends User> S save(S entity);
    List<User> findByName(String name);
}