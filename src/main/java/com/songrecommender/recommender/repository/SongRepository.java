package com.songrecommender.recommender.repository;

import com.songrecommender.recommender.model.Song;

import org.springframework.data.repository.CrudRepository;

public interface SongRepository extends CrudRepository<Song, Long> {
    
    <S extends Song> S save(S entity);
    <S extends Song> Iterable<S> saveAll(Iterable<S> entities);
    Iterable<Song> findAll();
}