package com.songrecommender.recommender.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.songrecommender.recommender.util.Trie;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Reference;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("playlist")
public class Playlist {
    @Id
    private Long id;

    @NotBlank
    private String name;

    @JsonIgnore
    private Map<String, Integer> count = new HashMap<String, Integer>();

    @JsonIgnore
    private Trie trie;

    @Reference
    @NotNull
    private User user;

    @Reference
    @NotNull
    private Set<Song> songs;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Song> getSongs() {
        return songs;
    }

    public void setSongs(Set<Song> songs) {
        this.songs = songs;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Playlist [id=" + id + ", name=" + name + ", songs=" + songs + ", user=" + user + "]";
    }

    public Trie getTrie() {
        return trie;
    }

    public void setTrie(Trie trie) {
        this.trie = trie;
    }

    public Map<String, Integer> getCount() {
        return count;
    }

    public void setCount(Map<String, Integer> count) {
        this.count = count;
    }

    public Playlist(@NotBlank String name, Map<String, Integer> count, Trie trie, @NotBlank User user,
            @NotBlank Set<Song> songs) {
        this.name = name;
        this.count = count;
        this.trie = trie;
        this.user = user;
        this.songs = songs;
    }

    public Playlist() {
    }

}