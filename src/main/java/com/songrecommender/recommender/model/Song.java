package com.songrecommender.recommender.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("song")
public class Song {
    @Id
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String singer;

    @NotBlank
    private String genre;

    @NotNull
    private Long tempo;

    public String getName() {
        return name;
    }

    public Long getTempo() {
        return tempo;
    }

    public void setTempo(Long tempo) {
        this.tempo = tempo;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Song [genre=" + genre + ", id=" + id + ", name=" + name + ", singer=" + singer + ", tempo=" + tempo
                + "]";
    }

    @Override
    public boolean equals(Object v) {
        boolean retVal = false; 
        if (v instanceof Song){
            Song ptr = (Song) v;
            retVal = ptr.id.longValue() == this.id;
        }

    return retVal;
  }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    public Song(@NotBlank String name, @NotBlank String singer, @NotBlank String genre, @NotNull Long tempo) {
        this.name = name;
        this.singer = singer;
        this.genre = genre;
        this.tempo = tempo;
    }

    public Song() {
    }

    
}