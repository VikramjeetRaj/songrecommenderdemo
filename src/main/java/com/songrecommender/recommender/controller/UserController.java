package com.songrecommender.recommender.controller;

import javax.validation.Valid;

import com.songrecommender.recommender.model.User;
import com.songrecommender.recommender.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/user")
public class UserController {

    @Autowired UserService userService;

    @PostMapping()
    public ResponseEntity<User> addUser(@RequestBody  @Valid User user) {
        User u = userService.save(user);
        return ResponseEntity.ok().body(u);
    }

}