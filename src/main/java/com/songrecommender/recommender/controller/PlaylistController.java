package com.songrecommender.recommender.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.songrecommender.recommender.dto.AddSongToPlaylistDto;
import com.songrecommender.recommender.model.Playlist;
import com.songrecommender.recommender.model.Song;
import com.songrecommender.recommender.service.PlaylistService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("api/v1/playlist")
public class PlaylistController {

    @Autowired
    private PlaylistService playlistService;
    
    @PostMapping()
    public ResponseEntity<Playlist> createPlaylist(@RequestBody @Valid Playlist playlist) {
        Playlist p = playlistService.createPlaylist(playlist);
        return ResponseEntity.ok().body(p);
    }

    @PutMapping()
    public ResponseEntity<Playlist> addSongToPlaylist(@RequestBody @Valid AddSongToPlaylistDto requestForm) {
        Playlist p = playlistService.addSongToPlaylist(requestForm.getPlaylistId(), requestForm.getSong());
        return ResponseEntity.ok().body(p);
    }

    @GetMapping()
    public ResponseEntity<Playlist> showPlaylist(@RequestParam @NotNull Long playlistId) {
        Playlist p = playlistService.showPlaylist(playlistId);
        return ResponseEntity.ok().body(p);
    }
    
    @GetMapping("recommend")
    public ResponseEntity<List<Song>> recommend(@RequestParam @NotBlank Long playlistId) {
        List<Song> song = playlistService.getRecommendation(playlistId);
        return ResponseEntity.ok().body(song);
    }
    
}