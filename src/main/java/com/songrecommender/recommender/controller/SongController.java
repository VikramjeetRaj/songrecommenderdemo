package com.songrecommender.recommender.controller;

import javax.validation.Valid;

import com.songrecommender.recommender.model.Song;
import com.songrecommender.recommender.service.SongService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/song")
public class SongController {

    @Autowired
    private SongService songService;

    @PostMapping()
    public ResponseEntity<Song> addSongToCatalogue(@RequestBody @Valid Song song) {
        return ResponseEntity.ok().body(songService.save(song));
    }
}