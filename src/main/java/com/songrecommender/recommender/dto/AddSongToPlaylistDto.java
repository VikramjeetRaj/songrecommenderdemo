package com.songrecommender.recommender.dto;

import javax.validation.constraints.NotNull;

import com.songrecommender.recommender.model.Song;

public class AddSongToPlaylistDto {

    @NotNull
    private Long playlistId;

    @NotNull
    private Song song;

    public AddSongToPlaylistDto(Long playlistId, Song song) {
        this.playlistId = playlistId;
        this.song = song;
    }

    public Long getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(Long playlistId) {
        this.playlistId = playlistId;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    @Override
    public String toString() {
        return "AddSongToPlaylistDto [playlistId=" + playlistId + ", song=" + song + "]";
    }

    public AddSongToPlaylistDto() {
    }
}