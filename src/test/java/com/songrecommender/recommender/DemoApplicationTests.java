package com.songrecommender.recommender;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.songrecommender.recommender.dto.AddSongToPlaylistDto;
import com.songrecommender.recommender.model.Playlist;
import com.songrecommender.recommender.model.Song;
import com.songrecommender.recommender.model.User;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DemoApplicationTests {

	@LocalServerPort
	private int port;
	
	TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();

	@Test
	void contextLoads() {
	}

	@Test
    public void testAddUser() throws Exception {
		headers.setContentType(MediaType.APPLICATION_JSON);
		final User user = new User();
		user.setName("name");
		final HttpEntity<User> entity = new HttpEntity<User>(user, headers);
		final ResponseEntity<User> response = restTemplate.postForEntity(createURLWithPort("/api/v1/user"), entity,
				User.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);
		assertEquals(response.getBody().getName(), user.getName());
	}

	@Test
	public void testAddSong() throws Exception {
		headers.setContentType(MediaType.APPLICATION_JSON);
		final Song song = new Song();
		song.setName("song1");
		song.setGenre("Rock");
		song.setTempo(70L);
		song.setSinger("singer");
		final HttpEntity<Song> entity = new HttpEntity<Song>(song, headers);
		final ResponseEntity<Song> response = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), entity,
				Song.class);

		assertEquals(response.getStatusCode(), HttpStatus.OK);
		assertEquals(response.getBody().getName(), song.getName());
		assertEquals(response.getBody().getTempo(), song.getTempo());
		assertEquals(response.getBody().getSinger(), song.getSinger());
		assertTrue(response.getBody().getId() != null);
	}

	@Test
	public void testCreatePlaylist() throws Exception {
		headers.setContentType(MediaType.APPLICATION_JSON);
		final Playlist playlist = new Playlist();

		playlist.setName("playlist");

		final User user = new User();
		user.setName("name");
		final HttpEntity<User> userEntity = new HttpEntity<User>(user, headers);
		final ResponseEntity<User> userResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/user"),
				userEntity, User.class);
		playlist.setUser(userResponse.getBody());

		final Set<Song> songs = new HashSet<Song>();
		Song song = new Song();
		song.setName("song1");
		song.setGenre("Rock");
		song.setTempo(60L);
		song.setSinger("singer");
		HttpEntity<Song> songEntity = new HttpEntity<Song>(song, headers);
		ResponseEntity<Song> songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity,
				Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song1");
		song.setGenre("Folk");
		song.setTempo(70L);
		song.setSinger("AB");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song2");
		song.setGenre("Rock");
		song.setTempo(70L);
		song.setSinger("DEF");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song3");
		song.setGenre("Country");
		song.setTempo(55L);
		song.setSinger("AB");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		playlist.setSongs(songs);

		final HttpEntity<Playlist> playlistEntity = new HttpEntity<Playlist>(playlist, headers);
		final ResponseEntity<Playlist> playlistResponse = restTemplate
				.postForEntity(createURLWithPort("/api/v1/playlist"), playlistEntity, Playlist.class);

		assertEquals(playlistResponse.getStatusCode(), HttpStatus.OK);
		assertEquals(playlistResponse.getBody().getName(), playlist.getName());
		assertEquals(playlistResponse.getBody().getUser().getName(), user.getName());
		assertEquals(playlistResponse.getBody().getName(), playlist.getName());
		assertEquals(playlistResponse.getBody().getSongs().size(), playlist.getSongs().size());
		assertTrue(playlistResponse.getBody().getId() != null);
	}

	@Test
	public void testAddSongToPlaylist() throws Exception {
		headers.setContentType(MediaType.APPLICATION_JSON);
		final Playlist playlist = new Playlist();

		playlist.setName("playlist");

		final User user = new User();
		user.setName("name");
		final HttpEntity<User> userEntity = new HttpEntity<User>(user, headers);
		final ResponseEntity<User> userResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/user"),
				userEntity, User.class);
		playlist.setUser(userResponse.getBody());

		final Set<Song> songs = new HashSet<Song>();
		Song song = new Song();
		song.setName("song1");
		song.setGenre("Rock");
		song.setTempo(60L);
		song.setSinger("singer");
		HttpEntity<Song> songEntity = new HttpEntity<Song>(song, headers);
		ResponseEntity<Song> songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity,
				Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song1");
		song.setGenre("Folk");
		song.setTempo(70L);
		song.setSinger("AB");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song2");
		song.setGenre("Rock");
		song.setTempo(70L);
		song.setSinger("DEF");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song3");
		song.setGenre("Country");
		song.setTempo(55L);
		song.setSinger("AB");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		playlist.setSongs(songs);

		final HttpEntity<Playlist> playlistEntity = new HttpEntity<Playlist>(playlist, headers);
		final ResponseEntity<Playlist> playlistResponse = restTemplate
				.postForEntity(createURLWithPort("/api/v1/playlist"), playlistEntity, Playlist.class);

		final AddSongToPlaylistDto addSongToPlaylistDto = new AddSongToPlaylistDto();
		addSongToPlaylistDto.setPlaylistId(playlistResponse.getBody().getId());
		song = new Song();
		song.setName("song4");
		song.setGenre("Rock");
		song.setTempo(60L);
		song.setSinger("XYZ");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		addSongToPlaylistDto.setSong(songResponse.getBody());

		final HttpEntity<AddSongToPlaylistDto> addSongToPlaylistDtoEntity = new HttpEntity<AddSongToPlaylistDto>(
				addSongToPlaylistDto, headers);
		final ResponseEntity<Playlist> addSongToPlaylistDtoResponse = restTemplate.exchange(
				createURLWithPort("/api/v1/playlist"), HttpMethod.PUT, addSongToPlaylistDtoEntity, Playlist.class);

		assertEquals(addSongToPlaylistDtoResponse.getStatusCode(), HttpStatus.OK);
		assertEquals(addSongToPlaylistDtoResponse.getBody().getName(), playlist.getName());
		assertEquals(addSongToPlaylistDtoResponse.getBody().getUser().getName(), user.getName());
		assertEquals(addSongToPlaylistDtoResponse.getBody().getName(), playlist.getName());
		assertEquals(addSongToPlaylistDtoResponse.getBody().getSongs().size(), playlist.getSongs().size() + 1);
		assertTrue(addSongToPlaylistDtoResponse.getBody().getId() != null);
	}

	@Test
	public void testRecommendPlaylist() throws Exception {
		headers.setContentType(MediaType.APPLICATION_JSON);
		final Playlist playlist = new Playlist();

		playlist.setName("playlist");

		final User user = new User();
		user.setName("name");
		final HttpEntity<User> userEntity = new HttpEntity<User>(user, headers);
		final ResponseEntity<User> userResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/user"),
				userEntity, User.class);
		playlist.setUser(userResponse.getBody());

		final Set<Song> songs = new HashSet<Song>();
		Song song = new Song();
		song.setName("song1");
		song.setGenre("Rock");
		song.setTempo(60L);
		song.setSinger("singer");
		HttpEntity<Song> songEntity = new HttpEntity<Song>(song, headers);
		ResponseEntity<Song> songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity,
				Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song1");
		song.setGenre("Folk");
		song.setTempo(70L);
		song.setSinger("AB");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song2");
		song.setGenre("Rock");
		song.setTempo(70L);
		song.setSinger("DEF");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song3");
		song.setGenre("Country");
		song.setTempo(55L);
		song.setSinger("AB");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		playlist.setSongs(songs);

		final HttpEntity<Playlist> playlistEntity = new HttpEntity<Playlist>(playlist, headers);
		final ResponseEntity<Playlist> playlistResponse = restTemplate
				.postForEntity(createURLWithPort("/api/v1/playlist"), playlistEntity, Playlist.class);

		final AddSongToPlaylistDto addSongToPlaylistDto = new AddSongToPlaylistDto();
		addSongToPlaylistDto.setPlaylistId(playlistResponse.getBody().getId());
		song = new Song();
		song.setName("song4");
		song.setGenre("Rock");
		song.setTempo(60L);
		song.setSinger("XYZ");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		addSongToPlaylistDto.setSong(songResponse.getBody());

		final HttpEntity<AddSongToPlaylistDto> addSongToPlaylistDtoEntity = new HttpEntity<AddSongToPlaylistDto>(
				addSongToPlaylistDto, headers);
		final ResponseEntity<Playlist> addSongToPlaylistDtoResponse = restTemplate.exchange(
				createURLWithPort("/api/v1/playlist"), HttpMethod.PUT, addSongToPlaylistDtoEntity, Playlist.class);

		final Map<String, Long> param = new HashMap<String, Long>();
		param.put("playlistId", addSongToPlaylistDtoResponse.getBody().getId());

		final ResponseEntity<Song[]> recommendationResponse = restTemplate
				.getForEntity(createURLWithPort("/api/v1/playlist/recommend") + "?playlistId="
						+ addSongToPlaylistDtoResponse.getBody().getId(), Song[].class);
		assertEquals(recommendationResponse.getStatusCode(), HttpStatus.OK);
	}

	@Test
	public void testShowPlaylist() throws Exception {
		headers.setContentType(MediaType.APPLICATION_JSON);
		final Playlist playlist = new Playlist();

		playlist.setName("playlist");

		final User user = new User();
		user.setName("name");
		final HttpEntity<User> userEntity = new HttpEntity<User>(user, headers);
		final ResponseEntity<User> userResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/user"),
				userEntity, User.class);
		playlist.setUser(userResponse.getBody());

		final Set<Song> songs = new HashSet<Song>();
		Song song = new Song();
		song.setName("song1");
		song.setGenre("Rock");
		song.setTempo(60L);
		song.setSinger("singer");
		HttpEntity<Song> songEntity = new HttpEntity<Song>(song, headers);
		ResponseEntity<Song> songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity,
				Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song1");
		song.setGenre("Folk");
		song.setTempo(70L);
		song.setSinger("AB");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song2");
		song.setGenre("Rock");
		song.setTempo(70L);
		song.setSinger("DEF");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		song = new Song();
		song.setName("song3");
		song.setGenre("Country");
		song.setTempo(55L);
		song.setSinger("AB");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		songs.add(songResponse.getBody());

		playlist.setSongs(songs);

		final HttpEntity<Playlist> playlistEntity = new HttpEntity<Playlist>(playlist, headers);
		final ResponseEntity<Playlist> playlistResponse = restTemplate
				.postForEntity(createURLWithPort("/api/v1/playlist"), playlistEntity, Playlist.class);

		final AddSongToPlaylistDto addSongToPlaylistDto = new AddSongToPlaylistDto();
		addSongToPlaylistDto.setPlaylistId(playlistResponse.getBody().getId());
		song = new Song();
		song.setName("song4");
		song.setGenre("Rock");
		song.setTempo(60L);
		song.setSinger("XYZ");
		songEntity = new HttpEntity<Song>(song, headers);
		songResponse = restTemplate.postForEntity(createURLWithPort("/api/v1/song"), songEntity, Song.class);
		addSongToPlaylistDto.setSong(songResponse.getBody());

		final HttpEntity<AddSongToPlaylistDto> addSongToPlaylistDtoEntity = new HttpEntity<AddSongToPlaylistDto>(
				addSongToPlaylistDto, headers);
		final ResponseEntity<Playlist> addSongToPlaylistDtoResponse = restTemplate.exchange(
				createURLWithPort("/api/v1/playlist"), HttpMethod.PUT, addSongToPlaylistDtoEntity, Playlist.class);

		final Map<String, Long> param = new HashMap<String, Long>();
		param.put("playlistId", addSongToPlaylistDtoResponse.getBody().getId());

		final ResponseEntity<Playlist> recommendationResponse = restTemplate.getForEntity(
				createURLWithPort("/api/v1/playlist") + "?playlistId=" + addSongToPlaylistDtoResponse.getBody().getId(),
				Playlist.class);
		assertEquals(recommendationResponse.getStatusCode(), HttpStatus.OK);
	}

	private String createURLWithPort(final String uri) {
        return "http://localhost:" + port + uri;
    }

}
